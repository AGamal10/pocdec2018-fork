const fetch = require("node-fetch");

let attendeesCnt = 0;
const MAC = [600001, 600002, 600003, 600004, 600005];
let curState = ['-1', '-1', '-1', '-1', '-1'];
let lastRecTime = new Date().getTime();

const stateColor = {
    '0': 'council__chair--disabled',
    '1': 'council__chair--active',
    '-1': 'council__chair--noData'
};

const getData = async () => {
    try {
        const response = await fetch('https://cloud.occupeye.com/OccupEye/GTel/api/SurveySensorsLatest/1?include_health=false');
        const json = await response.json();
        console.log(json);

        for (let item of json) {
            console.log(item.HardwareID);
            let i = MAC.indexOf(item.HardwareID);
            console.log('index', i);
            processState(i, item.LastTriggerType == 'Absent' ? '0' : '1');
        }
    } catch (error) {
        console.log(error);
    }
};
setInterval(getData, 5000);

function electState(count) {
    if (count >= 3) {
        document.getElementById('yesorno').classList = "";
        document.getElementById('yesorno').classList.add('yesStr');
        document.getElementById('yesorno').innerHTML = 'نعم';
    }
    else {
        document.getElementById('yesorno').classList = "";
        document.getElementById('yesorno').classList.add('noStr');
        document.getElementById('yesorno').innerHTML = 'لا';
    }
}

function processState(devIdx, devState) {
    lastRecTime = new Date().getTime();
    console.log(devIdx, curState[devIdx], devState);
    if (curState[devIdx] != devState) {
        if (devState == '1') attendeesCnt = attendeesCnt + 1;
        else if (curState[devIdx] != '-1') attendeesCnt = attendeesCnt - 1;
        if (attendeesCnt < 0) attendeesCnt = 0;
        if (attendeesCnt > 5) attendeesCnt = 5;
        curState[devIdx] = devState;
        document.getElementById(`s${devIdx + 1}`).classList = "";
        document.getElementById(`s${devIdx + 1}`).classList.add('council__chair');
        document.getElementById(`s${devIdx + 1}`).classList.add(stateColor[devState]);
        document.getElementById(`attendeesCnt`).innerHTML = `عدد الحضور: ${attendeesCnt}`;
        electState(attendeesCnt);
    }
}

function inactiveCheckLoop() {
    let curTime = new Date().getTime();
    if (curTime - lastRecTime > 15000) {
        for (let i = 0; i < 5; i++) {
            document.getElementById(`s${i + 1}`).classList = "";
            document.getElementById(`s${i + 1}`).classList.add('council__chair');
            document.getElementById(`s${i + 1}`).classList.add(stateColor['-1']);
            if (curState[i] == '1') attendeesCnt = attendeesCnt - 1;
            document.getElementById(`attendeesCnt`).innerHTML = `عدد الحضور: ${attendeesCnt}`;
            curState[i] = '-1';
            electState(attendeesCnt);
        }
    }
}

setInterval(inactiveCheckLoop, 1000);