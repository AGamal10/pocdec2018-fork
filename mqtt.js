var mqtt = require('mqtt')
var client  = mqtt.connect('mqtt://test.mosquitto.org')
const SPECDATAIDX = 4;
const EVENTIDX = 22;
const MACIDX = 1;

const MAC = ['0081F9889764', '0081F9889873', '0081F988940F', '0081F9889711', 
	'0081F988980B'];

const stateColor = {
	'0': 'council__chair--disabled',
	'1': 'council__chair--active',
	'-1': 'council__chair--noData'
};

let attendeesCnt = 0;
let curState = ['-1', '-1', '-1', '-1', '-1'];
let changeCnt = [0, 0, 0, 0, 0];
let lastRecTime = [0, 0, 0, 0, 0];

//initPositions();

client.on('connect', function () {
  client.subscribe('nakheel', function (err) {
    if (!err) {
      client.publish('nakheel', 'Hello sensor data')
    }
  })
})

client.on('message', function (topic, message) {
  // message is Buffer
	console.log(message.toString())
//	document.getElementById('mqttDev').innerHTML = message.toString();

	message = message.toString();
	if(message == 'Hello sensor data') return;
	else {
		message = message.split(',');
//		console.log(message);
		  
		const devMac = message[MACIDX];
		const devState = message[SPECDATAIDX][EVENTIDX];
		  
		const devIdx = MAC.indexOf(devMac);
		lastRecTime[devIdx] = new Date().getTime();
		console.log(devMac, devIdx, devState);
		
		processState(devIdx, devState);
	}
  //client.end()
})

for(let i = 0; i < 5; i++) lastRecTime[i] = new Date().getTime();

function inactiveCheckLoop() {
	for(let i = 0; i < 5; i++) {
		let curTime = new Date().getTime();
		if(curTime - lastRecTime[i] > 10000) {
			document.getElementById(`s${i + 1}`).classList = "";
			document.getElementById(`s${i + 1}`).classList.add('council__chair');
			document.getElementById(`s${i + 1}`).classList.add(stateColor['-1']);
			if(curState[i] == '1') attendeesCnt = attendeesCnt - 1;	
			document.getElementById(`attendeesCnt`).innerHTML = `عدد الحضور: ${attendeesCnt}`;			
			curState[i] = '-1';
			changeCnt[i] = 0;
			electState(attendeesCnt);
		}	
	}

}


setInterval(inactiveCheckLoop, 1000);

function processState(devIdx, devState) {
	console.log(devIdx, curState[devIdx], devState);
	if(curState[devIdx] != devState) {
		changeCnt[devIdx] = changeCnt[devIdx] + 1;
		if(changeCnt[devIdx] >= 3) {
			if(devState == '1') attendeesCnt = attendeesCnt + 1;
			else if(curState[devIdx] != '-1') attendeesCnt = attendeesCnt - 1;
			if(attendeesCnt < 0) attendeesCnt = 0;
			if(attendeesCnt > 5) attendeesCnt = 5;
			curState[devIdx] = devState;
			changeCnt[devIdx] = 0;
			document.getElementById(`s${devIdx + 1}`).classList = "";
			document.getElementById(`s${devIdx + 1}`).classList.add('council__chair');
			document.getElementById(`s${devIdx + 1}`).classList.add(stateColor[devState]);			
			document.getElementById(`attendeesCnt`).innerHTML = `عدد الحضور: ${attendeesCnt}`;
			electState(attendeesCnt);
		}
	}
	else changeCnt[devIdx] = 0;
		
}

function electState(count) {
	if(count >= 3) {
		document.getElementById('yesorno').classList = "";
		document.getElementById('yesorno').classList.add('yesStr');
		document.getElementById('yesorno').innerHTML = 'نعم';
	}
	else {
		document.getElementById('yesorno').classList = "";
		document.getElementById('yesorno').classList.add('noStr');
		document.getElementById('yesorno').innerHTML = 'لا';
	}
}
